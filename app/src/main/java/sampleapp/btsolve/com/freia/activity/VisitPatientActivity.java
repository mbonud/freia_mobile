package sampleapp.btsolve.com.freia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;

/**
 * Created by inBetween on 02/16/2018.
 */


public class VisitPatientActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    Bundle bundle;

    @BindView(R.id.datePicker)
    DatePicker datePicker;
    @BindView(R.id.notAvailable)
    MaterialSpinner notAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visit_patient);
        ButterKnife.bind(this);
        allControls();
    }

    private void pickList(){
        notAvailable.setItems("Poor Health", "Work Related", "No reason");
        notAvailable.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @OnClick(R.id.btnVisitPatientStart)
    public void btnVisitPatientStart(){
        Intent intent = new Intent(this, StartVideoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toolBarName", "Start Video");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.btnSetDate)
    public void btnSetDate(){
        new MaterialDialog.Builder(this)
                .title("Date Picker")
                .customView(R.layout.date_picker, false)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        String date = "" + datePicker.getMonth() + "-" + datePicker.getDayOfMonth() + "-" + datePicker.getYear();

                        Toast.makeText(VisitPatientActivity.this,date, Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void allControls(){
        toolbarControl();
        getBundleValue();
        pickList();
    } // end allcontrols

    // bundle value
    private void getBundleValue(){
        bundle = getIntent().getExtras();
        toolbar_title.setText(bundle.getString("toolBarName"));
    }

    // for toolbar methods
    private void toolbarControl(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
