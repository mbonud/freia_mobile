package sampleapp.btsolve.com.freia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.util.DialogUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;

/**
 * Created by inBetween on 02/15/2018.
 */

public class DashboardActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    Bundle bundle;

    private Thread thread;
    private void startThread(Runnable run) {
        if (thread != null) {
            thread.interrupt();
        }
        thread = new Thread(run);
        thread.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        allControls();
    }

    @OnClick(R.id.btnReset)
    public void btnRest(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnRecover)
    public void btnRecover(){
        new MaterialDialog.Builder(this)
                .title("Recovery")
                .content("Please wait...")
                .contentGravity(GravityEnum.CENTER)
                .progress(false, 100, true)
                .cancelListener(
                        dialog -> {
                            if (thread != null) {
                                thread.interrupt();
                            }
                        })
                .showListener(
                        dialogInterface -> {
                            final MaterialDialog dialog = (MaterialDialog) dialogInterface;
                            startThread(
                                    () -> {
                                        while (dialog.getCurrentProgress() != dialog.getMaxProgress()
                                                && !Thread.currentThread().isInterrupted()) {
                                            if (dialog.isCancelled()) {
                                                break;
                                            }
                                            try {
                                                Thread.sleep(50);
                                            } catch (InterruptedException e) {
                                                break;
                                            }
                                            dialog.incrementProgress(1);
                                        }
                                        runOnUiThread(
                                                () -> {
                                                    thread = null;
                                                    dialog.setContent("DONE");
    //                                                    dialog.hide();
                                                    new Timer().schedule(new TimerTask(){
                                                        public void run() {
                                                            nextIntent();
                                                            finish();
                                                        }
                                                    }, 1000 );
                                                });
                                    });
                        })
                .show();
    }


    private void nextIntent(){
        Intent intent = new Intent(this, PinCodeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toolBarName", "Pin Code");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.btnContinue)
    public void btnContinue(){
       nextIntent();
    }

    private void allControls(){
        toolbarControl();
        getBundleValue();
    } // end allcontrols

    // bundle value
    private void getBundleValue(){
        bundle = getIntent().getExtras();
        toolbar_title.setText(bundle.getString("toolBarName"));
    }

    // for toolbar methods
    private void toolbarControl(){
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
