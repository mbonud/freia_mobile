package sampleapp.btsolve.com.freia.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.tdf_title)
    TextView tdfTitle;
    @BindView(R.id.user_code)
    EditText user_code;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        btnLogin.setEnabled(false);

        SpannableString spanStr = new SpannableString(tdfTitle.getText().toString());
        spanStr.setSpan(new UnderlineSpan(), 0, spanStr.length(), 0);
        tdfTitle.setText(spanStr);

        user_code.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.toString().trim().length()==4){
                    btnLogin.setEnabled(true);
                } else {
                    btnLogin.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
    }

    @OnClick(R.id.btnLogin)
    public void btnLogin(){

        if(user_code.getText().toString().equals("1234")) {
            Intent intent = new Intent(this, DashboardActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("toolBarName", "Dashboard");
            intent.putExtras(bundle);
            startActivity(intent);
        }else{
            user_code.setText("");
            Toast.makeText(this, "Wrong user code", Toast.LENGTH_SHORT).show();
        }
    }
}
