package sampleapp.btsolve.com.freia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;
import sampleapp.btsolve.com.freia.adapter.ListViewAdapter;

/**
 * Created by inBetween on 02/16/2018.
 */

public class PatientDetails extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    Bundle bundle;

    @BindView(R.id.partnerName)
    MaterialEditText partnerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_details);
        ButterKnife.bind(this);
        allControls();
    }

    @OnClick(R.id.btnVisit)
    public void btnVisit(){
        Intent intent = new Intent(this, VisitPatientActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toolBarName", "Visit Patient");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void allControls(){
        toolbarControl();
        getBundleValue();
    } // end allcontrols

    // bundle value
    private void getBundleValue(){
        bundle = getIntent().getExtras();
        toolbar_title.setText(bundle.getString("toolBarName"));
        partnerName.setText(bundle.getString("partner"));
    }

    // for toolbar methods
    private void toolbarControl(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
