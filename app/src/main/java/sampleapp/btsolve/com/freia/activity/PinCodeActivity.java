package sampleapp.btsolve.com.freia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;
import sampleapp.btsolve.com.freia.preference.Preference;

/**
 * Created by inBetween on 02/15/2018.
 */

public class PinCodeActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    Bundle bundle;

    @BindView(R.id.pin_code)
    MaterialEditText pin_code;
    @BindView(R.id.confirm_pin_code)
    MaterialEditText confirm_pin_code;
    @BindView(R.id.hide_confirm_pin_code)
    TextInputLayout hide_confirm_pin_code;
    @BindView(R.id.tvTitle_pin_code)
    TextView tvTitle_pin_code;
    @BindView(R.id.tvPinCode)
    TextView tvPinCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pincode);
        ButterKnife.bind(this);
        allControls();
    }

    @OnClick(R.id.btnPincode)
    public void btnPincode(){
        if (new Preference(this).getPinCode() != null) {
            if(pin_code.getText().toString().equals(new Preference(this).getPinCode().toString())){
                nextIntent();
            }else{
                pin_code.setText("");
                Toast.makeText(this, "pin code not match", Toast.LENGTH_SHORT).show();
            }
        }else if(pin_code.getText().toString().trim().length() == 0 && confirm_pin_code.getText().toString().trim().length() == 0){
            Toast.makeText(this, "invalid pin code", Toast.LENGTH_SHORT).show();
            pin_code.setText("");
            confirm_pin_code.setText("");
        }else{
            if(pin_code.getText().toString().equals(confirm_pin_code.getText().toString())){
                new Preference(PinCodeActivity.this).getPinCode(pin_code.getText().toString());
                nextIntent();
            }else{
                pin_code.setText("");
                confirm_pin_code.setText("");
                Toast.makeText(this, "pin code not match", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void nextIntent(){
        Intent intent = new Intent(this, PatientListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toolBarName", "Patient List");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void checkPref() {
        if (new Preference(this).getPinCode() != null) {
            hide_confirm_pin_code.setVisibility(View.GONE);
            tvTitle_pin_code.setVisibility(View.GONE);
            tvPinCode.setText("Enter Pin");
        }
    }

    private void allControls(){
        toolbarControl();
        getBundleValue();
        checkPref();
    } // end allcontrols

    // bundle value
    private void getBundleValue(){
        bundle = getIntent().getExtras();
        toolbar_title.setText(bundle.getString("toolBarName"));
    }

    // for toolbar methods
    private void toolbarControl(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


