package sampleapp.btsolve.com.freia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;
import sampleapp.btsolve.com.freia.adapter.ListViewAdapter;

/**
 * Created by inBetween on 02/15/2018.
 */

public class PatientListActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    Bundle bundle;

    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    ListViewAdapter listViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daily_user);
        ButterKnife.bind(this);
        allControls();
    }

    @OnClick(R.id.btnEscalate)
    public void btnEscalate(){
        Intent intent = new Intent(this, EscalateActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toolBarName", "Escalate");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void allControls(){
        toolbarControl();
        getBundleValue();
        recyclerControl();
    } // end allcontrols

    private void recyclerControl(){
        recycleView.setHasFixedSize(true);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        listViewAdapter = new ListViewAdapter(this);
        recycleView.setAdapter(listViewAdapter);
    }

    // bundle value
    private void getBundleValue(){
        bundle = getIntent().getExtras();
        toolbar_title.setText(bundle.getString("toolBarName"));
    }

    // for toolbar methods
    private void toolbarControl(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
