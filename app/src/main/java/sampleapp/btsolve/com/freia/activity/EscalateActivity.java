package sampleapp.btsolve.com.freia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;

/**
 * Created by inBetween on 02/19/2018.
 */

public class EscalateActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.escalate);
        ButterKnife.bind(this);
        allControls();
    }

    @OnClick(R.id.btnEscalateBack)
    public void btnEscalateBack(){
        Intent intent = new Intent(this, PatientListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toolBarName", "Patient List");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void allControls(){
        toolbarControl();
        getBundleValue();
    } // end allcontrols

    // bundle value
    private void getBundleValue(){
        bundle = getIntent().getExtras();
        toolbar_title.setText(bundle.getString("toolBarName"));
    }

    // for toolbar methods
    private void toolbarControl(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
