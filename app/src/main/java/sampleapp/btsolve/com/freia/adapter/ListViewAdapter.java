package sampleapp.btsolve.com.freia.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sampleapp.btsolve.com.freia.R;
import sampleapp.btsolve.com.freia.activity.PatientDetails;

/**
 * Created by inBetween on 02/16/2018.
 */

public class ListViewAdapter extends RecyclerView.Adapter<ListViewAdapter.MyViewHolder> {
    private Context holderContext;
    private List<String> partnerList = new ArrayList<>();
    private List<String> dayList = new ArrayList<>();
    private List<String> scheduleList = new ArrayList<>();

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvPartner)
        TextView tvPartner;
        @BindView(R.id.tvDay)
        TextView tvDay;
        @BindView(R.id.tvSchedule)
        TextView tvSchedule;

        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.card_item_list)
        public void card_item_list(){
            String partner = partnerList.get(getPosition());
            String day = partnerList.get(getPosition());
            String schedule = partnerList.get(getPosition());
            Intent itemDetailed = new Intent(holderContext, PatientDetails.class);
            Bundle bundle = new Bundle();
            bundle.putString("partner", partner);
            bundle.putString("day", day);
            bundle.putString("schedule", schedule);
            bundle.putString("toolBarName", "Patient Details");
            itemDetailed.putExtras(bundle);
            holderContext.startActivity(itemDetailed);
            Toast.makeText(holderContext, partner, Toast.LENGTH_SHORT).show();
        }
    }

    public ListViewAdapter(Context context){
        this.holderContext = context;
        partnerList.add("Jose");
        partnerList.add("Louie");
        partnerList.add("Robin");
        partnerList.add("Medel");

        dayList.add("3");
        dayList.add("103");
        dayList.add("40");
        dayList.add("0");

        scheduleList.add("9:00 am");
        scheduleList.add("11:00 am");
        scheduleList.add("2:00 pm");
        scheduleList.add("9:00 pm");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(holderContext).inflate(R.layout.list_view, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvPartner.setText(partnerList.get(position));
        holder.tvDay.setText(dayList.get(position));
        holder.tvSchedule.setText(scheduleList.get(position));
    }

    @Override
    public int getItemCount() {
        return !partnerList.isEmpty() ? partnerList.size() : 0;
    }
}
