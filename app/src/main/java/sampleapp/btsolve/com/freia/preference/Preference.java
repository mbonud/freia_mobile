package sampleapp.btsolve.com.freia.preference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by inBetween on 02/15/2018.
 */

public class Preference {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor sharedPreferencesEditor;
    Context context;


    public Preference(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences("PreferenceApp", Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    public void getPinCode(String pinCode){
        sharedPreferencesEditor.putString("pinCode", pinCode);
        sharedPreferencesEditor.commit();
    }

    public String getPinCode(){
        return sharedPreferences.getString("pinCode", null);
    }

    public void clearData(){
        sharedPreferencesEditor.clear();
        sharedPreferencesEditor.commit();
    }
}
